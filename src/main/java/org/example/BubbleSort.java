package org.example;

public class BubbleSort {

    public static void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            boolean flag = false;
            if (array[i] < array[i - 1]) {
                int item = array[i];
                array[i] = array[i - 1];
                array[i - 1] = item;
                flag = true;
            }
            if (flag) {
                i = 0;
            }
        }
    }
}
