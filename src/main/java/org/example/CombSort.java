package org.example;

public class CombSort {

    public static void sort(int[] arr) {
        int gap = arr.length; // промежуточный интервал сортировки;
        boolean flag = false;

        while (!flag || gap != 1) {
            // если промежуточный интервал сортировки больше единицы - уменьшить интервал сортировки в процентном
            // соотношении на 10/13;
            if (gap > 1) {
                gap = gap * 10 / 13;
            } else {
                gap = 1; // в противном случае промежуточный интервал равен 1;
            }

            flag = true;
            for (int i = gap; i < arr.length; i++) {
                if (arr[i] < arr[i - gap]) {
                    int temp = arr[i];
                    arr[i] = arr[i - gap];
                    arr[i - gap] = temp;
                    flag = false;
                }
            }
        }
    }

}
