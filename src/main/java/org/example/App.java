package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

public class App {

    static Logger logger = LoggerFactory.getLogger("logback");
    private static final int NUMBER = 100000;
    private static final int GENERATED_BOUND = 100000;

    public static void main(String[] args) {

        TreeMap<Long, String> map = new TreeMap<>();

        createSort(map,() -> BubbleSort.sort(getRandomArray(NUMBER)), "BubbleSort - O(n^2)");
        createSort(map,() -> BubbleSortKnuth.sort(getRandomArray(NUMBER)), "Time BubbleSortKnuth - O(n^2)");
        createSort(map,() -> CombSort.sort(getRandomArray(NUMBER)), "Time CombSort - O(n^2)");

        int[] array = getRandomArray(NUMBER);
        createSort(map,() -> QuickSort.sort(array, array[0], array[array.length-1]), "Time QuickSort -  - O(n^2)");
        createSort(map,() -> SelectionSort.sortAscending(getRandomArray(NUMBER)),
                "Time SelectionSort(Ascending) -  - O(n^2)");
        createSort(map,() -> SelectionSort.sortDescending(getRandomArray(NUMBER)),
                "Time SelectionSort(Descending) -  - O(n^2)");
        createSort(map,() -> RadixSort.sort(getRandomArray(NUMBER)), "Time RadixSort(Descending) -  - O(nk)");

        int[] array1 = getRandomArray(NUMBER);
        int middleNum = array1[array1.length / 2];
        createSort(map,() -> MergeSort.sort(array1, middleNum), "Time MergeSort - O(n log(n))");
        print(map);
    }

    public static int[] getRandomArray(int number) {
        Random random = new Random();
        int[] array = new int[number];
        for (int element : array) {
            element = random.nextInt(GENERATED_BOUND);
        }
        return array;
    }

    public static void print(TreeMap<Long, String> map){
        Set<Long> setKeys = map.keySet();
        for(Long k: setKeys) {
            logger.info("{} = {}", map.get(k), k);
        }
    }

    public static void createSort(TreeMap<Long, String> map, SortOperations method, String sorter){
        long lastTime = System.nanoTime();
        method.sort();
        map.put(System.nanoTime() - lastTime, sorter);
    }
}
