package org.example;

@FunctionalInterface
public interface SortOperations {
    void sort();
}
