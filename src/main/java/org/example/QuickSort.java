package org.example;

public class QuickSort {

    public static void sort(int array[], int begin, int end)
    {
        if (begin < end) {
            int partitionIndex = partition(array, begin, end);

            // Recursively sort elements of the 2 sub-arrays
            sort(array, begin, partitionIndex-1);
            sort(array, partitionIndex+1, end);
        }
    }

    private static int partition(int arr[], int begin, int end)
    {
        int pivot = arr[end];
        int i = (begin-1);

        for (int j=begin; j<end; j++)
        {
            if (arr[j] <= pivot) {
                i++;

                int swapTemp = arr[i];
                arr[i] = arr[j];
                arr[j] = swapTemp;
            }
        }

        int swapTemp = arr[i+1];
        arr[i+1] = arr[end];
        arr[end] = swapTemp;

        return i+1;
    }
}
